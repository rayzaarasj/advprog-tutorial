package game;

public class Timer implements Runnable {

    private Thread thread;
    private boolean running = false;
    private Score score;

    public Timer(Score score) {
        this.score = score;
    }

    public void start() {
        if (thread == null) {
            running = true;
            thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void run() {
        while (running) {
            try {
                score.decrement();
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
    }
}
