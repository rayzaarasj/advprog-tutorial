package game;

import java.util.concurrent.atomic.AtomicInteger;

public class Score {

    private AtomicInteger score;

    public Score() {
        score = new AtomicInteger(100);
    }

    public void decrement() {
        score.decrementAndGet();
    }

    public void addPercentage(double percentage) {
        score.updateAndGet(x -> x + (int) (percentage * x));
    }

    public int value() {
        return  score.get();
    }
}
