package applicant;

import java.util.function.Predicate;

/**
 * 4th exercise.
 */
public class Applicant {

    public boolean isCredible() {
        return true;
    }

    public int getCreditScore() {
        return 700;
    }

    public int getEmploymentYears() {
        return 10;
    }

    public boolean hasCriminalRecord() {
        return true;
    }

    public static boolean evaluate(Applicant applicant, Predicate<Applicant> evaluator) {
        return evaluator.test(applicant);
    }

    public static String printEvaluate(boolean status) {
        String result = status ? "accepted" : "rejected";
        return "Result of evaluating applicant : " + result;
    }

    public static void main(String[] args) {
        Predicate<Applicant> creditCheck = app -> app.getCreditScore() > 600;
        Predicate<Applicant> employmentCheck = app -> app.getEmploymentYears() > 0;
        Predicate<Applicant> crimeCheck = app -> app.hasCriminalRecord();
        Predicate<Applicant> qualifiedCheck = app -> app.isCredible();


        Applicant applicant = new Applicant();
        System.out.println(printEvaluate(evaluate(applicant, qualifiedCheck.and(creditCheck))));
        System.out.println(printEvaluate(evaluate(applicant, qualifiedCheck.and(employmentCheck)
                .and(creditCheck))));
        System.out.println(printEvaluate(evaluate(applicant, qualifiedCheck.and(employmentCheck)
                .and(crimeCheck))));
        System.out.println(printEvaluate(evaluate(applicant, qualifiedCheck.and(employmentCheck)
                .and(creditCheck).and(crimeCheck))));
    }
}
