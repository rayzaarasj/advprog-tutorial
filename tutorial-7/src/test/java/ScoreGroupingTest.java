import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class ScoreGroupingTest {

    private HashMap<String, Integer> scores;

    @Before
    public void setUp() {
        scores = new HashMap<>();
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
    }

    @Test
    public void testGroupByScores() {
        HashMap<Integer, List<String>> test = (HashMap<Integer, List<String>>) ScoreGrouping.groupByScores(scores);
        assertTrue(test.get(11).contains("Charlie"));
        assertTrue(test.get(11).contains("Foxtrot"));
        assertTrue(test.get(12).contains("Alice"));
        assertTrue(test.get(15).contains("Bob"));
        assertTrue(test.get(15).contains("Delta"));
        assertTrue(test.get(15).contains("Emi"));
    }
}