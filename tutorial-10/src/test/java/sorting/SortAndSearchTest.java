package sorting;

import org.junit.Assert;
import org.junit.Test;

public class SortAndSearchTest {

    @Test
    public void testSlowSort() {
        int[] arr = new int[] {3,1,2,5,4};
        int[] expected = new int[] {1,2,3,4,5};
        Sorter.slowSort(arr);
        Assert.assertArrayEquals(expected, arr);
    }

    @Test
    public void testFastSort() {
        int[] arr = new int[] {3,1,2,5,4};
        int[] expected = new int[] {1,2,3,4,5};
        Sorter.fastSort(arr);
        Assert.assertArrayEquals(expected, arr);
    }

    @Test
    public void testSlowSearch() {
        int[] arr = new int[] {1,2,3,4,5};
        Assert.assertEquals(3, Finder.slowSearch(arr, 3));
    }
}
