package matrix;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class MatrixMultiplicationTest {
    private double[][] matrix1a;
    private double[][] matrix1b;
    private double[][] expected1;

    @Before
    public void setUp() throws IOException {
        matrix1a = Main.convertInputFileToMatrix(Main.pathFileMatrix1, Main.numberOfLine1,
                Main.numberOfLine1);
        matrix1b = Main.convertInputFileToMatrix(Main.pathFileMatrix2, Main.numberOfLine2,
                Main.numberOfLine1);
        expected1 = Main.convertInputFileToMatrix(Main.genericMatrixPath
                + "A/matrixResult.txt", 50, 50);
    }

    @Test
    public void basicMultiplication() throws InvalidMatrixSizeForMultiplicationException {
        assertArrayEquals(MatrixOperation.basicMultiplicationAlgorithm(matrix1a, matrix1b),
                expected1);
    }

    @Test
    public void strassenMultiplication() throws InvalidMatrixSizeForMultiplicationException {
        assertArrayEquals(MatrixOperation.strassenMatrixMultiForNonSquareMatrix(matrix1a,
                matrix1b), expected1);
    }

    @Test
    public void convertNonSquareMatrix() throws IOException {
        Main.convertInputFileToMatrix(Main.genericMatrixPath + "B/matrix10rows50columns.txt",
                50, 10);
    }


}
