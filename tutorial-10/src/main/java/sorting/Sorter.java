package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    public static int[] fastSort(int[] inputArr) {
        inputArr = helperSort(inputArr, 0, inputArr.length - 1);
        return inputArr;
    }

    private static int[] helperSort(int[] inputArr, int low, int high) {

        if (low < high) {
            int partition = partition(inputArr, low, high);

            helperSort(inputArr, low, partition - 1);
            helperSort(inputArr, partition + 1, high);
        }

        return inputArr;
    }

    private static int partition(int[] inputArr, int low, int high) {
        int pivot = inputArr[high];
        int wall = low  - 1;

        for (int i = low; i < high; i++) {
            if (inputArr[i] <= pivot) {
                wall++;

                int temp = inputArr[wall];
                inputArr[wall] = inputArr[i];
                inputArr[i] = temp;
            }
        }

        int temp = inputArr[wall + 1];
        inputArr[wall + 1] = inputArr[high];
        inputArr[high] = temp;

        return wall + 1;
    }
}
