package tutorial.javari;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavariControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void javariAnimalNotExist() throws Exception {
        this.mockMvc.perform(get("/javari"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is("Animal not found!")));
    }

    @Test
    public void javariAnimalWithIdNotExist() throws Exception {
        this.mockMvc.perform(get("/javari/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is("Animal not found!")));
    }

    @Test
    public void javariDeleteAnimalWithIdNotExist() throws Exception {
        this.mockMvc.perform(delete("/javari/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is("Animal not found!")));
    }

}