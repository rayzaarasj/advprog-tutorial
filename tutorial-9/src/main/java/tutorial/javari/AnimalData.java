package tutorial.javari;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class AnimalData {

    private AtomicInteger counter;
    private final String path = "tutorial-9/src/main/java/tutorial/javari/animal_records.csv";
    private ArrayList<Animal> animals = new ArrayList<>();

    public AnimalData() {
        BufferedReader br = null;
        int max = -1;
        try {
            br = new BufferedReader(new FileReader(path));
            String line;
            String[] parse;

            while ((line = br.readLine()) != null) {
                parse = line.split(",");
                int id = Integer.parseInt(parse[0]);
                String type = parse[1];
                String name = parse[2];
                String gen = parse[3];
                double length = Double.parseDouble(parse[4]);
                double weight = Double.parseDouble(parse[5]);
                String con = parse[6];

                Gender gender = Gender.parseGender(gen);
                Condition condition = Condition.parseCondition(con);

                max = (max > id) ? max : id;

                animals.add(new Animal(id,type,name,gender,length,weight,condition));
            }

            counter = new AtomicInteger(max++);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Animal addAnimal(
            String type,
            String name,
            Gender gen,
            double length,
            double weight,
            Condition con
    ) {
        Animal animal = new Animal(
                counter.incrementAndGet(),
                type,
                name,
                gen,
                length,
                weight,
                con
        );
        animals.add(animal);
        saveCSV();
        return animal;
    }

    public int getSize() {
        return this.animals.size();
    }

    public ArrayList<Animal> getAnimals() {
        return animals;
    }

    public Animal findAnimal(int id) {
        Animal ret = null;
        for (Animal animal : animals) {
            if (animal.getId() == id) {
                ret = animal;
                break;
            }
        }
        return ret;
    }

    public Animal deleteAnimalWithId(int id) {
        Animal animal = null;
        for (int i = 0; i < animals.size(); i++) {
            if (animals.get(i).getId() == id) {
                animal = animals.remove(i);
            }
        }
        saveCSV();
        return animal;
    }

    public void saveCSV() {
        try {
            FileWriter fw = new FileWriter(path, false);

            StringBuilder sb = new StringBuilder();
            for (Animal animal : animals) {
                sb.append(animal.getId() + ",");
                sb.append(animal.getType() + ",");
                sb.append(animal.getName() + ",");

                switch (animal.getGender()) {
                    case MALE:
                        sb.append("male,");
                        break;
                    case FEMALE:
                        sb.append("female,");
                        break;
                    default:
                        break;
                }

                sb.append(animal.getLength() + ",");
                sb.append(animal.getWeight() + ",");

                switch (animal.getCondition()) {
                    case HEALTHY:
                        sb.append("healthy");
                        break;
                    case SICK:
                        sb.append("not healthy");
                        break;
                    default:
                        break;
                }

                sb.append("\n");
            }

            fw.write(sb.toString());

            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
