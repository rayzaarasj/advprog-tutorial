package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class StuffedCrustDough implements Dough {
    @Override
    public String toString() {
        return "Stuffed with goodness crust dough";
    }
}
