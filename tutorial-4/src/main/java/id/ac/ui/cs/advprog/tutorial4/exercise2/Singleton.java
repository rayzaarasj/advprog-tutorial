package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    private static Singleton singelton;

    public static Singleton getInstance() {
        if (singelton == null) {
            singelton = new Singleton();
        }

        return singelton;
    }

    private Singleton() {

    }
}
