package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.TempeCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.StuffedCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PeanutSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Kangkung;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    @Override
    public Dough createDough() {
        return new StuffedCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new PeanutSauce();
    }

    @Override
    public Cheese createCheese() {
        return new TempeCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Kangkung(), new BlackOlives(), new Eggplant(), new Spinach()};
        return veggies;
    }

    @Override
    public Clams createClam() {
        return null;
    }
}
