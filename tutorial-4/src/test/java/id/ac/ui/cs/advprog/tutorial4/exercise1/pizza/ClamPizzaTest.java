package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClamPizzaTest extends PizzaTest {
    private ClamPizza clamPizza;

    @Before
    @Override
    public void setUp() {
        super.setUp();
        when(clamsMock.toString()).thenReturn("Mock clams");
        when(pizzaIngredientFactoryMock.createClam()).thenReturn(clamsMock);
        clamPizza = new ClamPizza(pizzaIngredientFactoryMock);
        clamPizza.setName("Clams pizza");
    }

    @Test
    public void prepareTest( ){
        clamPizza.prepare();
        assertEquals(clamPizza.dough.toString(), doughMock.toString());
        assertEquals(clamPizza.sauce.toString(), sauceMock.toString());
        assertEquals(clamPizza.cheese.toString(), cheeseMock.toString());
        assertEquals(clamPizza.clam.toString(), clamsMock.toString());
    }
}
