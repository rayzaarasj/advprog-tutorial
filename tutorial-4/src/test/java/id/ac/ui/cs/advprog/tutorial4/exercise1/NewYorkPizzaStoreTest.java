package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    NewYorkPizzaStore nyStore;

    @Before
    public void setUp() {
        nyStore = new NewYorkPizzaStore();
    }

    @Test
    public void createPizzaTest() {
        Pizza pizza;

        pizza = nyStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());

        pizza = nyStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza.getName());

        pizza = nyStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza.getName());
    }
}
