package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;


public class DepokPizzaStoreTest {
    DepokPizzaStore dpStore;

    @Before
    public void setUp() {
        dpStore = new DepokPizzaStore();
    }

    @Test
    public void createPizzaTest() {
        Pizza pizza;

        pizza = dpStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());

        pizza = dpStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());

        pizza = dpStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());
    }
}
