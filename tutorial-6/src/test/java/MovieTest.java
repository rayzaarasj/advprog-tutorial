import org.junit.Before;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class MovieTest {

    private Movie movie1;
    private Movie movie2;

    @Before
    public void setUp() {
        movie1 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Who Killed Captain John?", Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie1.getTitle());
    }

    @Test
    public void setTitle() {
        movie1.setTitle("Bad Black");

        assertEquals("Bad Black", movie1.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie1.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie1.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie1.getPriceCode());
    }

    @Test
    public void equalsItself() {
        assertTrue(movie1.equals(movie1));
    }

    @Test
    public void equalsNull() {
        assertFalse(movie1.equals(null));
    }

    @Test
    public void equalsOtherMovie() {
        assertFalse(movie1.equals(movie2));
    }

    @Test
    public void testHash() {
        assertEquals(Objects.hash(movie1.getTitle(), movie1.getPriceCode()), movie1.hashCode());
    }
}