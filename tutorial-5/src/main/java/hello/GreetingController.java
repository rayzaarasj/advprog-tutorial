package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false)
                                       String name, Model model) {
        if (name == null || name.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", "Hello, " + name + "! I hope you interested to hire me");
        }

        model.addAttribute("name", "My name is Rayza Arasj Mahardhika");
        model.addAttribute(
                "desc",
                "I'm currently pursuing a bachelor's degree on computer science");
        model.addAttribute("birth", "I was born at Bogor on 26th of August 1998");
        model.addAttribute(
                "address",
                "Currently i live at my apartment which called Taman Melati Margonda");
        model.addAttribute("uni", "Fasilkom UI 2016-2020");
        model.addAttribute("sma", "SMAN 1 Bogor 2013-2016");
        model.addAttribute("smp", "SMPN 1 Bogor 2010-2013");
        model.addAttribute("sd", "SD Bina Insani Bogor 2004-2010");

        return "greeting";
    }

}
